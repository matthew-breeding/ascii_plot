# -*- coding: utf-8 -*- 
### ascii_plot.py
# 
# Created Nov 2020
# Matthew Breeding
# matthew.l.breeding@vanderbilt.edu

import numpy as np
from os import popen

### TODO: Error bars, native MRED histograms
### TODO: Axes labels

class AsciiPlot:
    """AsciiPlot creates histogram plots in the terminal window from a set of x-y data."""

    def __init__(self, xy_data = [],  **kwargs):
        self._xy_data = xy_data
        self._defaults = {
             'logx': True,
             'logy': True,
             'loglog': False,
             'xlen': 120, ### Length of the overall plot in x/y in the terminal ## TODO : Only for multiples of 10
             'ylen': 25,
             'title': "",
             'integrate': False, 
             'xlabel': "",
             'ylabel': "",
             'ytick_int': 3,         #interval between yticks
             'left_pad': 11,         ## TODO  fix
             'barchar': "▓",         #"▒",
             'spacechar' : " ", 
             'sci': True, 
             'decimal_points' : 2,
             'norm' : 1,
             'xmin' : 0,
             'xmax' : 0,
             'ymin': 0, 
             'ymax': 0,
             '_yframe_string' : "|",
             '_ytick_string' : "┼",
             '_yticks' : 0,
             'frame': True}
        self.restore_defaults()
        for k, v in kwargs.items():
            setattr(self, k, v)
        self.load(xy_data)

### TODO: rows, columns = os.popen('stty size', 'r').read().split() ### Auto-resize based on the terminal window size
        
    def normalize(self, norm_factor):
        self.norm = norm_factor
        self._update_ylim(self.ymin / self.norm, self.ymax / self.norm)

    def restore_defaults(self):
        self.__set_defaults()
        if self._xy_data != []:
            self.set_x_limits_from_data()
            self.set_y_limits_from_data()

    _num_format_str = property(lambda self: ".{}E".format(self.decimal_points) if self.sci else ".{}f".format(self.decimal_points))
    _leftpad_string = property(lambda self: " " * self.left_pad)
    _ytick = property(lambda self: self._ytick_string if self._yticks % self.ytick_int == 0 else self._yframe_string)

    def set_xlim(self, val1, val2):
        self.xmin, self.xmax = val1, val2
    def set_ylim(self, val1, val2):
        self.ymin, self.ymax = val1, val2

    xlim = property(lambda self: [self.xmin, self.xmax])
    ylim = property(lambda self: [self.ymin, self.ymax])


    def load(self, xy_data, **kwargs):
        self._xy_data = xy_data

        if  self.xlim == [0,0]:
            self.set_x_limits_from_data()
        if self.ylim == [0,0]:
            self.set_y_limits_from_data()

        self._values =  np.concatenate(np.array([np.tile(x, int(round(y))) for x, y in self._xy_data]))  ### TODO: Fix for weights < 1

    def _print_title(self, title = ""): 
        if not title:
            title = self.title
        print("\n" * 2)
        lines = title.split("\n")
        for line in lines:
            print(self._leftpad_string + self.spacechar * int(self.xlen / 2 - len(line)/2) + line)
        if self.frame:
            print(" "* self.left_pad + "┌"+ "─"*(self.xlen - 1 )+ "┐")

    def show(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

        self._xbins =  np.logspace(np.log10(self.xmin), np.log10(self.xmax), self.xlen) if self.logx else np.linspace(self.xmin, self.xmax, self.xlen)
        self._histogram_data = np.histogram(self._values / self.norm, bins = self._xbins)[0] 

        if self.integrate:
            self.ymax = sum(self._histogram_data)
            self._histogram_data = self._histogram_data[::-1].cumsum()[::-1]
        self._ybins =  np.logspace(np.log10(self.ymin), np.log10(max(self._histogram_data)), self.ylen) if self.logy else np.linspace(self.ymin, max(self._histogram_data), self.ylen) 
        self._print_title()

        ### y-values
        for row in range(self.ylen, 0,-1):
            ytick_label = format(self._ybins[row - 1], self._num_format_str) if self._ytick != self._yframe_string else " "

            printstring = "{:>10s} {}".format(ytick_label, self._ytick)

            for hist_value in self._histogram_data:
                if hist_value >= self._ybins[row - 1]:
                    printstring += self.barchar
                else:
                    printstring += self.spacechar
            if self.frame:
                printstring += self._ytick

            print(printstring)
            self._yticks += 1

        ### Bottom x-axis printing
        tick_xstring = self._leftpad_string
        xtick_labelstring = self._leftpad_string
        for xx in range(0, self.xlen, 10):
            tick_xstring += "┼"
            tick_xstring += "─" * 9
            xtick_labelstring += format(format(self._xbins[xx], self._num_format_str) , "<9s")
            xtick_labelstring += self.spacechar
        print(tick_xstring  + "┼")
        print(xtick_labelstring + format(self._xbins[-1], self._num_format_str))


    def set_x_limits_from_data(self, fit_to_nonzero_data = False): ##TODO
        xmin = min([x[0] for x in self._xy_data if x[0] > 0]) if self.logx else min([x[0] for x in self._xy_data ]) 
        xmax = max([x[0] for x in self._xy_data])
        xmin = float(xmax) / 10 if xmin == xmax else xmin
        if self.logx:
            xmin = xmax / 100 if xmin == 0 else xmin
        self._update_xlim(xmin, xmax)

    def set_y_limits_from_data(self):
        ymin = min([x[1] for x in self._xy_data if x[1] > 0]) if self.logy else min([x[1] for x in self._xy_data ]) 
        ymax = max([y[1] for y in self._xy_data])
        ymin = float(ymax) / 10 if ymin == ymax else ymin
        if self.logy:
            ymin = ymax / 100 if ymin == 0 else ymin
        self._update_ylim(ymin, ymax)

    def __set_defaults(self):
        for k, v in self._defaults.items():
            setattr(self, k, v)
        if self.loglog:
            self.logx = True
            self.logy = True

    def _update_xlim(self, _min, _max):
        self.xmin, self.xmax = _min, _max
    def _update_ylim(self, _min, _max):
        self.ymin, self.ymax = _min, _max
