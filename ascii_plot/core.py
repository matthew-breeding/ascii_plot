from os import get_terminal_size
import numpy as np
from config import rcParams as rc
from colorama import init, Fore, Back
init(autoreset=True)  ## Initialize the colorama package
base_str = Fore.RESET + Back.RESET + ' ' ## Defautl text/background color
rc["top_pad"] = 12
rc["right_pad"] = 24

class Canvas(object):
    def __init__(self, nrows = None, ncols = None, x0 = 0, y0 = 0):
        if not ncols:
            ncols = get_terminal_size()[0] - 1
        if not nrows:
            nrows = get_terminal_size()[1] - 1

        self.m = ncols
        self.n = nrows

        self.x0, self.y0 = x0, y0
        self.grid = np.zeros((nrows, ncols), dtype = object)
        self.grid[self.grid==0] = base_str

    def get_subgrid(self, start, size):
        start_row, start_col = start
        end_row, end_col = start_row + size[0], start_col + size[1]
        return self.grid[start_row:end_row, start_col:end_col]

    def draw(self):
        for x in self.grid:
            print(''.join(x))

# Master Canvas
mc = Canvas()
ax_start = (rc['left_pad'], rc['top_pad'])
ax_size =  (mc.n - rc['top_pad'] - rc['bottom_pad'], 
            mc.m - rc['right_pad'] - rc['left_pad'])

ax_grid = mc.get_subgrid(ax_start, ax_size)

class Frame(object):
    def __init__(self, ax_grid):
        self.grid = ax_grid
        self.bold = True 
        self.ticks_on = True
        self.n, self.m  = self.grid.shape
        self._x_ticks_step = self.m // 4 + 1 ## TODO -- allow user to change
        self._y_ticks_step = self.n // 4 + 1 ## TODO -- allow user to change
        self.ticks_bold = True 
        self.color = Fore.YELLOW  ## TODO -- allow for lowercase, other colors, autosetting?
        self._configure_frame()
        self._set_ticks()
        self.data_grid = self.grid[1:-1,1:-1]

    def _configure_frame(self):
        if self.bold:
            self._corner_top_right =  "\N{BOX DRAWINGS HEAVY DOWN AND LEFT}"
            self._corner_top_left =  "\N{BOX DRAWINGS HEAVY DOWN AND RIGHT}"
            self._corner_bottom_right =  "\N{BOX DRAWINGS HEAVY UP AND LEFT}"
            self._corner_bottom_left = "\N{BOX DRAWINGS HEAVY UP AND RIGHT}"
        else:
            self._corner_top_right =  "\N{BOX DRAWINGS LIGHT DOWN AND LEFT}"
            self._corner_top_left =  "\N{BOX DRAWINGS LIGHT DOWN AND RIGHT}"
            self._corner_bottom_right =  "\N{BOX DRAWINGS LIGHT UP AND LEFT}"
            self._corner_bottom_left =  "\N{BOX DRAWINGS LIGHT UP AND RIGHT}"

        if self.ticks_bold and self.ticks_on:
            self._corner_top_right = self._corner_top_left = self._corner_bottom_right = self._corner_bottom_left = "\N{BOX DRAWINGS HEAVY VERTICAL AND HORIZONTAL}"

    def _set_ticks(self):

        self.grid[0] = self.color  + "\N{BOX DRAWINGS HEAVY HORIZONTAL}"
        self.grid[-1] = self.color  + "\N{BOX DRAWINGS HEAVY HORIZONTAL}"
        self.grid.T[0] = self.color  + "\N{BOX DRAWINGS HEAVY VERTICAL}"
        self.grid.T[-1] = self.color  + "\N{BOX DRAWINGS HEAVY VERTICAL}"
        if self.ticks_on:
            if self.ticks_bold:
                self.grid[0][0:-1:self._x_ticks_step] = self.color  + "\N{BOX DRAWINGS HEAVY UP AND HORIZONTAL}"
                self.grid[-1][0:-1:self._x_ticks_step] = self.color  + "\N{BOX DRAWINGS HEAVY DOWN AND HORIZONTAL}"
                self.grid.T[0][0:-1:self._y_ticks_step] = self.color  + "\N{BOX DRAWINGS HEAVY VERTICAL AND LEFT}"
                self.grid.T[-1][0:-1:self._y_ticks_step] = self.color  + "\N{BOX DRAWINGS HEAVY VERTICAL AND RIGHT}"
            else:
                self.grid[0][0:-1:self._x_ticks_step] = self.color  + "\N{BOX DRAWINGS UP LIGHT AND HORIZONTAL HEAVY}"
                self.grid[-1][0:-1:self._x_ticks_step] = self.color  + "\N{BOX DRAWINGS DOWN LIGHT AND HORIZONTAL HEAVY}"
                self.grid.T[0][0:-1:self._y_ticks_step] = self.color  + "\N{BOX DRAWINGS VERTICAL HEAVY AND LEFT LIGHT}"
                self.grid.T[-1][0:-1:self._y_ticks_step] = self.color  + "\N{BOX DRAWINGS VERTICAL HEAVY AND RIGHT LIGHT}"

        self._set_corners() ## require that corners are re-set

    def _set_corners(self):
        self.grid[0][0]   = self.color + self._corner_top_left
        self.grid[-1][0]  = self.color + self._corner_bottom_left
        self.grid[0][-1]  = self.color + self._corner_top_right
        self.grid[-1][-1] = self.color + self._corner_bottom_right


class DataGrid(object):
    def __init__(self, data_grid, x= np.array([]), y = np.array([]), marker = "#", color = "RED"):
        self.grid = data_grid
        self.x = x
        self.y = y
        self._x = None
        self._y = None
        self.color = Fore.__dict__[color.upper()]
        self.marker = marker
        self.xmin  = min(self.x) if self.x.size else 0
        self.ymin  = min(self.y) if self.y.size else 0
        self.xmax  = max(self.x) if self.x.size else 0
        self.ymax  = max(self.y) if self.y.size else 0

    def set_x(self, x):
        self.x = x
        self.xmin = min(x)
        self.xmax = max(x)

    def set_y(self, y):
        self.y = y
        self.ymin = min(y)
        self.ymax = max(y)

    def set_data(self, x,y):
        self.set_x(x)
        self.set_y(y)

    def set_xy(self, xy):
        self.set_x([x[0] for x in xy])
        self.set_y([x[1] for x in xy])

    ### TODO -- add buffer/ adjust limits?
    def _format_data(self):
        #adjust min to zero
        self._x = self.x - min(self.x)
        self._y = self.y - min(self.y)

        # normalize to [0,1]
        self._x = self._x / max(self._x) 
        self._y = self._y / max(self._y)

        # adjust to matrix shape
        self._x = (self._x * (self.grid.shape[1] -2)).astype(int) 
        self._y = (self._y * (self.grid.shape[0] -2)).astype(int) 

        
        for x, y in zip(self._x, self._y):
            self.grid[y][x] = self.color + self.marker

ax = Frame(ax_grid)


x_data = np.linspace(-2*np.pi, 2*np.pi, 100) 
sin_data = np.sin(x_data)
cos_data = np.cos(x_data)
dg = DataGrid(ax.data_grid, x_data, sin_data)._format_data()
dg2 = DataGrid(ax.data_grid, x_data, cos_data, marker = "@", color = "green")._format_data()

